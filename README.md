# Local Development Infrastructure

```
make up
```

---
### MongoExpress
http://localhost:8081/

* ID: admin
* PW: Password1

---
### RabbitMQ
http://localhost:15672/

---
* ID: guest
* PW: guest

---
### Flower
http://localhost:5555/

---
### RedisCommander
http://localhost:18081/

---
### Adminer
http://localhost:8080/
* SV: mariadb
* ID: root
* PW: z9hG4bK
* DB: mariadb

---
### DynamodbAdmin
http://localhost:8001/


npx postgraphile -c 'postgres://postgres:postgres@localhost/postgres' --watch --enhance-graphiql --dynamic-json