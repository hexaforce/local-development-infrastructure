show:
	docker ps -a
	docker volume ls
	docker network ls
	docker images

upf:
	docker compose up --build

up:
	docker compose up -d --build

down:
	docker compose down

stop:
	docker stop `docker ps -a -q`
	docker rm `docker ps -a -q`

init:
	docker volume prune
	docker network prune

destroy:
	docker system prune -a --volumes

log:
	docker compose logs -f

# mongo:
# 	docker compose --profile mongo-service up

# queue:
# 	docker compose --profile queue-service up

# proxy:
# 	docker compose --profile proxy-service up

# rds:
# 	docker compose --profile rds-service up

# dynamo:
# 	docker compose --profile dynamo-service up

# basic:
# 	docker compose --profile queue-service --profile rds-service up
